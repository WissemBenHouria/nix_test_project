#!/bin/bash

# Docker build and run commands
docker build -t nix-app .
docker run -d -p 3000:3000 --name nix-container nix-app
