# Stage 1: Build Nix image
FROM nixos/nix
# Set up a default shell.nix for the project
WORKDIR /app

COPY shell.nix .

# Copy the rest of your application
COPY . .

# Build the Nix environment
RUN nix-shell --run 'npm install' 

## just type the commands you need to run and start your project

# Install Node.js dependencies
#RUN npm install

# Expose the necessary port
EXPOSE 3000

# Start the application

CMD nix-shell --run "npm run start"
#CMD ["nix-shell","--run","npm run start"] 

